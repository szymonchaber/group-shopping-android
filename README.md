![Logo](assets/group_shopping.jpg)
# Group Shopping App
[![Build Status](https://travis-ci.org/szymonchaber/group-shopping-android.svg?branch=master)](https://travis-ci.org/szymonchaber/group-shopping-android)

<a href="https://play.google.com/store/apps/details?id=pl.szymonchaber.groupshopping">
  <img alt="Android app on Google Play" src="http://developer.android.com/images/brand/en_generic_rgb_wo_60.png" />
</a>