package pl.szymonchaber.groupshopping.tracking

object FirebaseParams {

    const val IS_LOGGED_IN = "is_logged_in"
    const val LIST_TITLE_LENGTH = "list_title_length"
    const val IS_TAKEN: String = "is_taken"
    const val LISTS_COUNT: String = "lists_count"
    const val ITEM_NAME_LENGTH: String = "item_name_length"
}
