package pl.szymonchaber.groupshopping.tracking

object CustomEvents {

    const val CLICK_ADD_SHOPPING_LIST: String = "click_add_shopping_list"
    const val ADD_SHOPPING_LIST = "add_shopping_list"
    const val MENU_LOGIN_CLICK = "menu_login_click"
    const val LOGOUT: String = "logout"
    const val VIEW_SHOPPING_LIST: String = "view_shopping_list"
    const val ADD_ITEM: String = "add_item"
    const val DELETE_ITEM: String = "delete_item"
    const val CLICK_ADD_USER: String = "click_add_to_user"
    const val CLICK_ITEM: String = "click_item"
    const val ITEM_CHECKED: String = "item_taken_changed"
    const val CLICK_ADD_ITEM: String = "click_add_item"
    const val ADD_USER: String = "add_user"
    const val DELETE_SHOPPING_LIST: String = "delete_shopping_list"
}
