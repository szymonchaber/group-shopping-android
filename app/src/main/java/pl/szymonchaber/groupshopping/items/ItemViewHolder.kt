package pl.szymonchaber.groupshopping.items

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.common.model.Item

class ItemViewHolder(
        view: View,
        private val onCheckedChange: (position: Int, checked: Boolean) -> Unit,
        private val onItemClick: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.name)
    private val isTaken: CheckBox = view.findViewById(R.id.isTakenCheckbox)
    val viewForeground: View = view.findViewById(R.id.view_foreground)

    init {
        viewForeground.setOnClickListener { onItemClick.invoke(adapterPosition) }
    }

    fun bind(item: Item) {
        isTaken.setOnCheckedChangeListener(null)
        name.text = item.name
        isTaken.isChecked = item.isTaken
        isTaken.setOnCheckedChangeListener { _, isChecked -> onCheckedChange.invoke(adapterPosition, isChecked) }
    }
}
