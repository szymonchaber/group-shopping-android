package pl.szymonchaber.groupshopping.items.api.model

data class EditItem(val name: String, val isTaken: Boolean = false, val order: Int?)
