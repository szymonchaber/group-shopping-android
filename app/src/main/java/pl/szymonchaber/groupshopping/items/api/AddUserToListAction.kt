package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.login.api.model.UserNet

class AddUserToListAction(credentialsManager: CredentialsManager, private val shoppingListId: String) :
    AuthorizedAction(credentialsManager) {

    fun getAddUserObservable(user: String): Observable<Any> {
        return create(ShoppingListService::class.java)
            .addUserToList(shoppingListId, UserNet(user))
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}
