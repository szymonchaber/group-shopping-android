package pl.szymonchaber.groupshopping.items.repository

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.mapper.ShoppingListMapper
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.items.api.AddItemAction
import pl.szymonchaber.groupshopping.items.api.AddUserToListAction
import pl.szymonchaber.groupshopping.items.api.DeleteItemAction
import pl.szymonchaber.groupshopping.items.api.EditItemAction
import pl.szymonchaber.groupshopping.items.api.GetShoppingListAction

class NetworkShoppingListDetailsRepository(
        listId: String,
        private val credentialsManager: CredentialsManager,
        private val getShoppingListAction: GetShoppingListAction = GetShoppingListAction(
                credentialsManager,
                listId
        ),
        private val addItemAction: AddItemAction = AddItemAction(
                credentialsManager,
                listId
        ),
        private val editItemAction: EditItemAction = EditItemAction(
                credentialsManager,
                listId
        ),
        private val deleteItemAction: DeleteItemAction = DeleteItemAction(
                credentialsManager,
                listId
        ),
        private val addUserToListAction: AddUserToListAction = AddUserToListAction(
                credentialsManager,
                listId
        )
) : ShoppingListDetailsRepository(listId) {

    override fun getShoppingList(): Observable<ShoppingList> {
        return getShoppingListAction.getShoppingListObservable().map { ShoppingListMapper.fromNetworkShoppingList(it) }
    }

    override fun addItem(newItemName: String): Observable<Any> {
        return addItemAction.getAddItemObservable(newItemName)
    }

    override fun editItem(item: Item, taken: Boolean, newPosition: Int): Observable<Any> {
        return editItemAction.getEditItemObservable(item, taken, order = newPosition)
    }

    override fun deleteItem(itemId: String): Observable<Any> {
        return deleteItemAction.getDeleteItemObservable(itemId)
    }

    override fun addUser(user: String): Observable<Any> {
        return addUserToListAction.getAddUserObservable(user)
    }
}
