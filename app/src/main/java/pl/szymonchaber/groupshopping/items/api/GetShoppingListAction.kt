package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.common.api.model.ShoppingListNet

class GetShoppingListAction(credentialsManager: CredentialsManager, private val shoppingListId: String) :
    AuthorizedAction(credentialsManager) {

    fun getShoppingListObservable(): Observable<ShoppingListNet> {
        return create(ShoppingListService::class.java).getShoppingList(shoppingListId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}


