package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.items.api.model.EditItem
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path

internal interface ItemService {

    @POST("lists/{listId}/items/")
    fun addItem(@Path("listId") listId: String, @Body item: EditItem): Observable<Any>

    @PATCH("lists/{listId}/items/{itemId}/")
    fun editItem(
        @Path("listId") listId: String, @Path("itemId") itemId: String, @Body item: EditItem
    ): Observable<Any>

    @DELETE("lists/{listId}/items/{itemId}/")
    fun deleteItem(@Path("listId") listId: String, @Path("itemId") itemId: String): Observable<Any>
}
