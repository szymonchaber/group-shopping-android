package pl.szymonchaber.groupshopping.items.api.model

data class EditShoppingList(val title: String)
