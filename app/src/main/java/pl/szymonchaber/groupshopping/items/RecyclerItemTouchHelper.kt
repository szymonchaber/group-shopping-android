package pl.szymonchaber.groupshopping.items

import android.graphics.Canvas
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.szymonchaber.groupshopping.lists.ListsAdapter

class RecyclerItemTouchHelper(
    private val listener: RecyclerItemTouchHelperListener,
    private val dragEnabled: Boolean = false
) : ItemTouchHelper.Callback() {

    override fun isLongPressDragEnabled(): Boolean {
        return dragEnabled
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder
    ): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: ViewHolder, target: ViewHolder): Boolean {
        listener.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        listener.onItemDismiss(viewHolder, viewHolder.adapterPosition)
    }

    override fun onSelectedChanged(viewHolder: ViewHolder?, actionState: Int) {
        viewHolder?.let {
            if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                val foregroundView = getForegroundView(it)
                getDefaultUIUtil().onSelected(foregroundView)
            }
        }
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            listener.onDragStart()
        }
    }

    override fun onChildDrawOver(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val foregroundView = getForegroundView(viewHolder)
            getDefaultUIUtil().onDrawOver(
                canvas, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive
            )
        } else {
            super.onChildDrawOver(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val foregroundView = getForegroundView(viewHolder)

            getDefaultUIUtil()
                .onDraw(canvas, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
        } else {
            super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: ViewHolder) {
        val foregroundView = getForegroundView(viewHolder)
        getDefaultUIUtil().clearView(foregroundView)
    }

    private fun getForegroundView(viewHolder: ViewHolder): View {
        return when (viewHolder) {
            is ItemViewHolder -> viewHolder.viewForeground
            is ListsAdapter.ListViewHolder -> viewHolder.viewForeground
            else                                                     -> {
                throw Exception()
            }
        }
    }

    interface RecyclerItemTouchHelperListener {

        fun onItemDismiss(viewHolder: ViewHolder, position: Int)

        fun onItemMove(fromPosition: Int, toPosition: Int)

        fun onDragStart()
    }
}
