package pl.szymonchaber.groupshopping.items.edit

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import pl.szymonchaber.groupshopping.R

class AddUserToListDialog : AppCompatDialogFragment() {

    private lateinit var editText: EditText
    private var addUserClickListener: OnAddUserClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
                .setTitle(R.string.list_add_user)
                .setView(createView())
                .setPositiveButton(R.string.action_add) { _, _ ->
                    addUserClickListener?.onAddUser(editText.text.toString())
                }
                .setNegativeButton(R.string.action_cancel, null)
                .create()
                .apply {
                    window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
                }
    }

    fun setActionsListener(listener: OnAddUserClickListener) {
        addUserClickListener = listener
    }

    private fun createView(): View {
        return requireActivity().layoutInflater.inflate(R.layout.dialog_add_user, null, false).also {
            editText = it.findViewById(R.id.itemNameEditText)
        }
    }

    interface OnAddUserClickListener {

        fun onAddUser(user: String)
    }
}
