package pl.szymonchaber.groupshopping.items

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.common.model.Item
import java.util.ArrayList

class ItemsAdapter(
    private val context: Context,
    private val onItemActionListener: OnItemActionListener
) : RecyclerView.Adapter<ItemViewHolder>() {

    private var data: MutableList<Item> = ArrayList()

    fun newData(newItems: MutableList<Item>) {
        val oldItems = data
        data = newItems
        DiffUtil.calculateDiff(ItemsDiffCallback(oldItems, newItems)).dispatchUpdatesTo(this)
    }

    fun removeItem(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: Item, position: Int) {
        data.add(position, item)
        notifyItemInserted(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item, parent, false),
                { adapterPosition, checked ->
                    onItemActionListener.onTakenChanged(data[adapterPosition], checked)
                },
                { adapterPosition ->
                    onItemActionListener.onItemClicked(data[adapterPosition])
                }
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    companion object {

        class ItemsDiffCallback(private val old: List<Item>, private val new: List<Item>) : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return old[oldItemPosition].id == new[newItemPosition].id
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return old[oldItemPosition] == new[newItemPosition]
            }

            override fun getOldListSize() = old.size

            override fun getNewListSize() = new.size
        }
    }
}
