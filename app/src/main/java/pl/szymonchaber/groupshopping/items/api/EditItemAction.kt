package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.items.api.model.EditItem

class EditItemAction(
    credentialsManager: CredentialsManager,
    val listId: String

) : AuthorizedAction(credentialsManager) {

    fun getEditItemObservable(
        item: Item,
        taken: Boolean = item.isTaken,
        name: String = item.name,
        order: Int = item.order
    ): Observable<Any> {
        return create(ItemService::class.java)
            .editItem(listId, item.id, EditItem(name, taken, order))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
