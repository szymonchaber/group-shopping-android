package pl.szymonchaber.groupshopping.items

import pl.szymonchaber.groupshopping.common.model.Item

interface OnItemActionListener {

    fun onTakenChanged(item: Item, isTaken: Boolean)
    fun onItemClicked(item: Item)
}
