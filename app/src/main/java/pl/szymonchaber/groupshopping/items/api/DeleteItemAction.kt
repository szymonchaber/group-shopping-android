package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsProvider
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction

class DeleteItemAction(credentialsProvider: CredentialsProvider, private val listId: String) :
    AuthorizedAction(credentialsProvider) {

    fun getDeleteItemObservable(itemId: String): Observable<Any> {
        return create(ItemService::class.java)
            .deleteItem(listId, itemId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
