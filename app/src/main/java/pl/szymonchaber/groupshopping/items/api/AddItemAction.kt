package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.items.api.model.EditItem

class AddItemAction(
    credentialsManager: CredentialsManager,
    private val shoppingListId: String
) : AuthorizedAction(credentialsManager) {

    fun getAddItemObservable(title: String): Observable<Any> {
        return create(ItemService::class.java)
            .addItem(shoppingListId, EditItem(title, order = null))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
