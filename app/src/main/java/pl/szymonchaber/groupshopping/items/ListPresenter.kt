package pl.szymonchaber.groupshopping.items

import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.common.model.User
import pl.szymonchaber.groupshopping.common.rx.InterruptEvent
import pl.szymonchaber.groupshopping.common.rx.PauseResumeTransformer
import pl.szymonchaber.groupshopping.items.repository.ShoppingListDetailsRepository
import java.util.Collections
import java.util.concurrent.TimeUnit

class ListPresenter(
        private val view: ListDetailView,
        private val credentialsManager: CredentialsManager,
        private val listId: String,
        private val repository: ShoppingListDetailsRepository = ShoppingListDetailsRepository.from(credentialsManager, listId)
) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val pauseResumeSubject: PublishSubject<InterruptEvent> = PublishSubject.create()
    private var loadSubject: PublishSubject<Any> = PublishSubject.create()
    private val moveSubject: PublishSubject<MoveAction> = PublishSubject.create()

    private var items: List<Item> = ArrayList()
    private var users: List<User> = ArrayList()

    private var loadingListDisposable: Disposable? = null

    private var isActivityPaused: Boolean = false

    init {
        subscribeLoading()
        resume()
        subscribeMoving()
        load()
        if (credentialsManager.isLoggedIn()) {
            startPooling()
        }
    }

    private fun subscribeLoading() {
        loadingListDisposable = loadSubject
            .doOnNext { view.hideError() }
            .flatMap {
                repository.getShoppingList()
            }
            .compose(PauseResumeTransformer(pauseResumeSubject))
            .subscribe(this::onLoaded, this::onError)
            .also { compositeDisposable.add(it) }
    }

    private fun subscribeMoving() {
        moveSubject
            .debounce(1, TimeUnit.SECONDS)
            .doOnNext { pause() }
            .doAfterNext { resume() }
            .flatMap { repository.moveItem(it) }
            .doOnComplete { resume() }
            .subscribe({ load() }, { load() })
            .also { compositeDisposable.add(it) }
    }

    private fun onError(throwable: Throwable) {
        resume()
        view.hideContent()
        view.hideProgress()
        view.showError()
        view.onError(throwable)
        Log.e("ListPresenter", "error", throwable)
    }

    fun getItems() = items

    fun getUsers() = users

    fun restoreState(items: List<Item>, users: List<User>) {
        onLoaded(items, users)
    }

    private fun onLoaded(it: ShoppingList) {
        onLoaded(it.items, it.users)
    }

    private fun onLoaded(newItems: List<Item>, newUsers: List<User>) {
        items = newItems
        users = newUsers
        view.hideProgress()
        view.hideError()
        view.showContent()
        view.show(items, newUsers)
        if (items.isEmpty()) {
            view.showEmptyView()
        } else {
            view.hideEmptyView()
        }
    }

    fun onEditItem(newItemName: String, itemId: String) {
        val item: Item? = items.find { it.id == itemId }
        item?.let {
            repository.editItemName(item, newItemName)
                .doOnSubscribe { pause() }
                .doOnComplete {
                    resume()
                    load()
                }
                .subscribe({ load() }, { load() })
        }
    }

    fun onTakenChanged(item: Item, taken: Boolean) {
        repository.setItemTaken(item, taken)
            .doOnSubscribe { pause() }
            .doOnComplete {
                resume()
                load()
            }
            .subscribe({ load() }, { load() })
    }

    fun onAddItem(newItemName: String) {
        repository.addItem(newItemName)
            .doOnSubscribe { pause() }
            .doOnComplete {
                resume()
                load()
            }
            .subscribe({ load() }, {
                resume()
                load()
                view.showUnableToAddItem()
                Log.e("ListPresenter", "Error adding item", it)
            }).also {
                compositeDisposable.add(it)
            }
    }

    fun onAddUser(user: String) {
        repository.addUser(user)
            .doOnSubscribe { pause() }
            .doOnComplete {
                resume()
                load()
            }
            .subscribe({}, {
                resume()
                load()
                view.showUnableToAddUser()
                Log.e("ListPresenter", "Unable to add user:", it)
            })
            .also {
                compositeDisposable.add(it)
            }
    }

    fun onMoveItem(fromPosition: Int, toPosition: Int) {
        moveSubject.onNext(MoveAction(items[fromPosition], toPosition))
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(items, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(items, i, i - 1)
            }
        }
        onLoaded(items, users)
    }

    fun load() {
        if (loadingListDisposable?.isDisposed == true) {
            subscribeLoading()
        }
        resume()
        loadSubject.onNext(Any())
    }

    private fun startPooling() {
        compositeDisposable.add(Observable.interval(2, TimeUnit.SECONDS)
            .subscribe { loadSubject.onNext(Any()) })
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onDeleteItem(itemPosition: Int) {
        pause()
        val itemToDelete = items[itemPosition]
        view.showItemDeleted(itemPosition)
        view.showUndoSnackbar(itemToDelete, itemPosition)
    }

    fun onDeleteItemSnackbarDismissed(item: Item) {
        performDeleteRequest(item.id)
    }

    private fun performDeleteRequest(itemId: String) {
        repository.deleteItem(itemId)
            .doOnSubscribe { pause() }
            .doOnComplete {
                resume()
                load()
            }
            .doOnError {
                resume()
                load()
            }
            .subscribe(
                {
                    load()
                },
                {
                    view.deleteItemFailed()
                }).also {
                compositeDisposable.add(it)
            }
    }

    fun undoDeleting() {
        resume()
        load()
    }

    fun onDragStart() {
        pause()
    }

    private fun pause() {
        pauseResumeSubject.onNext(InterruptEvent.PAUSE)
    }

    private fun resume() {
        pauseResumeSubject.onNext(InterruptEvent.RESUME)
    }

    fun onResume() {
        if (isActivityPaused) {
            resume()
        }
        isActivityPaused = false
    }

    fun onPause() {
        isActivityPaused = true
        pause()
    }
}

