package pl.szymonchaber.groupshopping.items

import pl.szymonchaber.groupshopping.common.model.Item

data class MoveAction(val item: Item, val target: Int)
