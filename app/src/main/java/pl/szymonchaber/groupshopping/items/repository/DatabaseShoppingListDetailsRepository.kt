package pl.szymonchaber.groupshopping.items.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.GroupShopping
import pl.szymonchaber.groupshopping.common.mapper.ShoppingListMapper
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.database.model.ItemDb
import pl.szymonchaber.groupshopping.database.model.ShoppingListWithItems

class DatabaseShoppingListDetailsRepository(listId: String) : ShoppingListDetailsRepository(listId) {

    private val shoppingListDao = GroupShopping.shoppingListDao()

    override fun getShoppingList(): Observable<ShoppingList> {
        return localList()
                ?.map { ShoppingListMapper.fromDatabaseShoppingList(it) }
            ?.filter { it.id == listId } ?: Observable.error(Error())
    }

    private fun localList(): Observable<ShoppingListWithItems>? {
        return shoppingListDao?.getShoppingLists()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.flatMapObservable { Observable.fromIterable(it) }
    }

    override fun addItem(newItemName: String): Observable<Any> {
        return shoppingListDao?.getItems()
            ?.toObservable()
            ?.map {
                it.size
            }?.map {
                val itemDb = ItemDb(name = newItemName, listId = listId.toLong(), order = it)
                shoppingListDao.insertItem(itemDb)
                Any()

            }?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread()) ?: Observable.just(Any())
    }

    override fun editItem(item: Item, taken: Boolean, newPosition: Int): Observable<Any> {
        val itemDb = itemDb(item, taken, listId, newPosition)
        return Observable.create<Any> {
            shoppingListDao?.updateItem(itemDb)
            it.onNext(Any())
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun itemDb(
        item: Item,
        taken: Boolean,
        listId: String,
        order: Int
    ) = ItemDb(item.id.toLong(), item.name, taken, item.created.orEmpty(), listId.toLong(), order)

    override fun deleteItem(itemId: String): Observable<Any> {
        return Observable.create<Any> {
            shoppingListDao?.deleteItem(itemId.toLong())
            it.onNext(Any())
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun addUser(user: String): Observable<Any> {
        return Observable.error(UnsupportedOperationException("User not logged in cannot add other users"))
    }
}
