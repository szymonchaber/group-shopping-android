package pl.szymonchaber.groupshopping.items

import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.User

interface ListDetailView {

    fun show(
        items: List<Item>,
        users: List<User>
    )

    fun onError(throwable: Throwable)
    fun showError()
    fun hideError()
    fun hideContent()
    fun showContent()
    fun hideProgress()
    fun showProgress()
    fun showUnableToAddItem()
    fun showUnableToAddUser()
    fun showItemDeleted(itemPosition: Int)
    fun showUndoSnackbar(item: Item, itemPosition: Int)
    fun deleteItemFailed()
    fun showEmptyView()
    fun hideEmptyView()
    fun hideRetryButton()
}
