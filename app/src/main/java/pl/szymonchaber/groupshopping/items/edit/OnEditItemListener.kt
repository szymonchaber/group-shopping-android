package pl.szymonchaber.groupshopping.items.edit

interface OnEditItemListener {

    fun onAddItem(itemName: String)
    fun onEditItem(newItemName: String, itemId: String)
}
