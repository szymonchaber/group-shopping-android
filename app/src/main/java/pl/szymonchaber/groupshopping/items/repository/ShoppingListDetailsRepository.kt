package pl.szymonchaber.groupshopping.items.repository

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.items.MoveAction

abstract class ShoppingListDetailsRepository(val listId: String) {

    abstract fun getShoppingList(): Observable<ShoppingList>
    abstract fun addItem(newItemName: String): Observable<Any>
    abstract fun editItem(item: Item, taken: Boolean, newPosition: Int): Observable<Any>
    abstract fun deleteItem(itemId: String): Observable<Any>
    abstract fun addUser(user: String): Observable<Any>

    fun moveItem(it: MoveAction): Observable<Any> {
        return editItem(it.item, taken = it.item.isTaken, newPosition = it.target)
    }

    fun setItemTaken(item: Item, taken: Boolean): Observable<Any> {
        return editItem(item, taken, item.order)
    }

    fun editItemName(item: Item, newItemName: String): Observable<Any> {
        return editItem(Item(item.created, item.isTaken, newItemName, item.id, item.order), item.isTaken, item.order)
    }

    companion object {

        fun from(credentialsManager: CredentialsManager, listId: String): ShoppingListDetailsRepository {
            return if (credentialsManager.isLoggedIn()) NetworkShoppingListDetailsRepository(
                listId,
                credentialsManager
            ) else DatabaseShoppingListDetailsRepository(listId)
        }
    }
}

