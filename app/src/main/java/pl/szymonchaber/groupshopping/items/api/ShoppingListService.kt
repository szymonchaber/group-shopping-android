package pl.szymonchaber.groupshopping.items.api

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.common.api.model.ShoppingListNet
import pl.szymonchaber.groupshopping.login.api.model.UserNet
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

internal interface ShoppingListService {

    @GET("lists/{id}/")
    fun getShoppingList(@Path("id") id: String): Observable<ShoppingListNet>

    @POST("lists/{id}/users/")
    fun addUserToList(@Path("id") shoppingListId: String, @Body user: UserNet): Observable<Any>
}
