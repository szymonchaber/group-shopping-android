package pl.szymonchaber.groupshopping.items

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_list.addItemButton
import kotlinx.android.synthetic.main.activity_list.content
import kotlinx.android.synthetic.main.activity_list.emptyView
import kotlinx.android.synthetic.main.activity_list.listRecyclerView
import kotlinx.android.synthetic.main.activity_list.progress
import kotlinx.android.synthetic.main.activity_list.retry
import kotlinx.android.synthetic.main.activity_list.swipeRefreshLayout
import kotlinx.android.synthetic.main.activity_list.toolbar
import kotlinx.android.synthetic.main.activity_list.usersList
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.common.model.User
import pl.szymonchaber.groupshopping.items.edit.AddUserToListDialog
import pl.szymonchaber.groupshopping.items.edit.AddUserToListDialog.OnAddUserClickListener
import pl.szymonchaber.groupshopping.items.edit.EditItemDialog
import pl.szymonchaber.groupshopping.items.edit.OnEditItemListener
import pl.szymonchaber.groupshopping.login.LoginActivity
import pl.szymonchaber.groupshopping.tracking.CustomEvents
import pl.szymonchaber.groupshopping.tracking.FirebaseParams
import java.io.Serializable

class ListDetailActivity : AppCompatActivity(),
    ListDetailView, OnItemActionListener,
    OnEditItemListener, OnAddUserClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var firebaseAnalytics : FirebaseAnalytics

    private lateinit var presenter: ListPresenter
    private lateinit var itemsAdapter: ItemsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        showProgress()
        addItemButton.setOnClickListener {
            showAddItemDialog()
        }
        retry.setOnClickListener {
            hideContent()
            showProgress()
            presenter.load()
        }
        swipeRefreshLayout.setOnRefreshListener {

            if (retry.visibility == View.VISIBLE) {
                hideContent()
                hideRetryButton()
                swipeRefreshLayout.isRefreshing = false
                showProgress()
            }
            presenter.load()
        }
        itemsAdapter = ItemsAdapter(this, this)
        listRecyclerView.adapter = itemsAdapter
        listRecyclerView.addItemDecoration(
            androidx.recyclerview.widget.DividerItemDecoration(
                this,
                androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
            )
        )
        setupSwipeToDelete()

        (intent.getSerializableExtra(EXTRA_SHOPPING_LIST) as ShoppingList).let {
            presenter = ListPresenter(this, CredentialsManager(this), it.id)
            title = it.title
            usersList.removeAllViews()
            showUsers(it.users)
        }

        savedInstanceState?.let {
            restoreInstanceState(it)
        } ?: presenter.load()

        firebaseAnalytics.logEvent(CustomEvents.VIEW_SHOPPING_LIST, null)
    }

    override fun onResume() {
        presenter.onResume()
        super.onResume()
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("items", presenter.getItems() as Serializable)
        outState.putSerializable("users", presenter.getUsers() as Serializable)
    }

    private fun restoreInstanceState(savedInstanceState: Bundle) {
        presenter.restoreState(
            savedInstanceState.getSerializable("items") as List<Item>,
            savedInstanceState.getSerializable("users") as List<User>
        )
    }

    private fun setupSwipeToDelete() {
        val itemTouchHelperCallback = RecyclerItemTouchHelper(this, true)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(listRecyclerView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.addUserToList -> {
                showAddUserToListDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onItemDismiss(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ItemViewHolder) {
            firebaseAnalytics.logEvent(CustomEvents.DELETE_ITEM, null)
            presenter.onDeleteItem(viewHolder.adapterPosition)
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        presenter.onMoveItem(fromPosition, toPosition)
    }

    override fun onDragStart() {
        presenter.onDragStart()
    }

    private fun showAddUserToListDialog() {
        firebaseAnalytics.logEvent(CustomEvents.CLICK_ADD_USER, null)
        if (!CredentialsManager(this).isLoggedIn()) {
            showLogInSnackbar()
            return
        }
        AddUserToListDialog()
            .also {
                it.setActionsListener(this)
            }.show(supportFragmentManager, "AddUserToListDialog")
    }

    private fun showLogInSnackbar() {
        Snackbar.make(content, getString(R.string.login_required_to_add_users), Snackbar.LENGTH_LONG)
                .setAction(R.string.login) {
                    startActivity(Intent(this, LoginActivity::class.java))
                }
                .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.list_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onItemClicked(item: Item) {
        firebaseAnalytics.logEvent(CustomEvents.CLICK_ITEM, null)
        EditItemDialog.create(item.id, item.name).also {
            it.setActionsListener(this)
        }.show(supportFragmentManager, "EditItemDialog")
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun show(items: List<Item>, users: List<User>) {
        showContent()
        showUsers(users)
        itemsAdapter.newData(items.toMutableList())
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showItemDeleted(itemPosition: Int) {
        itemsAdapter.removeItem(itemPosition)
    }

    override fun showUndoSnackbar(item: Item, itemPosition: Int) {
        Snackbar.make(content, formatItemName(item.name), Snackbar.LENGTH_LONG)
            .setAction(R.string.undo) {
                itemsAdapter.restoreItem(item, itemPosition)
                presenter.undoDeleting()
            }.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    if (event != DISMISS_EVENT_ACTION) presenter.onDeleteItemSnackbarDismissed(item)
                }
            }).setActionTextColor(Color.YELLOW).show()
    }

    private fun formatItemName(name: String): String {
        return if (name.length < 15) getString(R.string.item_deleted_plain, name) else
            getString(
                R.string.item_deleted_ellipsized, name.substring(0, 15)
            )
    }

    override fun deleteItemFailed() {
        showSnackbar(R.string.list_unable_to_delete_item)
    }

    private fun showUsers(users: List<User>) {
        usersList.removeAllViews()
        showUser(getString(R.string.you))

        users.map {
            it.username
        }.filter {
            it != CredentialsManager(this).getUsername()
        }.forEach {
            showUser(it)
        }
    }

    private fun showUser(it: String) {
        usersList.addView(
            layoutInflater.inflate(R.layout.user, usersList, false).apply {
                this.findViewById<TextView>(R.id.userName).text = it
            })
    }

    override fun onAddItem(itemName: String) {
        firebaseAnalytics.logEvent(CustomEvents.ADD_ITEM, Bundle().apply {
            putInt(FirebaseParams.ITEM_NAME_LENGTH, itemName.length)
        })
        presenter.onAddItem(itemName)
    }

    override fun onEditItem(newItemName: String, itemId: String) {
        presenter.onEditItem(newItemName, itemId)
    }

    override fun onTakenChanged(item: Item, isTaken: Boolean) {
        firebaseAnalytics.logEvent(CustomEvents.ITEM_CHECKED, Bundle().apply {
            putBoolean(FirebaseParams.IS_TAKEN, isTaken)
        })
        presenter.onTakenChanged(item, isTaken)
    }

    override fun onAddUser(user: String) {
        firebaseAnalytics.logEvent(CustomEvents.ADD_USER, null)
        presenter.onAddUser(user)
    }

    private fun showAddItemDialog() {
        firebaseAnalytics.logEvent(CustomEvents.CLICK_ADD_ITEM, null)
        EditItemDialog()
            .also {
                it.setActionsListener(this)
            }.show(supportFragmentManager, "AddItemDialog")
    }

    override fun onError(throwable: Throwable) {
        showError()
        showSnackbar(R.string.server_error)
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showContent() {
        listRecyclerView.visibility = View.VISIBLE
    }

    override fun hideContent() {
        listRecyclerView.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun hideRetryButton() {
        retry.visibility = View.GONE
    }

    override fun showError() {
        retry.visibility = View.VISIBLE
    }

    override fun hideError() {
        retry.visibility = View.GONE
    }

    override fun showUnableToAddItem() {
        showSnackbar(R.string.list_unable_to_add_item)
    }

    override fun showUnableToAddUser() {
        showSnackbar(R.string.list_unable_to_add_user)
    }

    private fun showSnackbar(@StringRes text: Int) {
        Snackbar.make(findViewById(R.id.content), text, Snackbar.LENGTH_SHORT).show()
    }

    companion object {

        private const val EXTRA_SHOPPING_LIST: String = "list"

        fun createIntent(context: Context, shoppingList: ShoppingList): Intent =
            Intent(context, ListDetailActivity::class.java).putExtra(EXTRA_SHOPPING_LIST, shoppingList)
    }
}
