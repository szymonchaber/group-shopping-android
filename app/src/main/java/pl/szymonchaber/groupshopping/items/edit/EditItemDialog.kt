package pl.szymonchaber.groupshopping.items.edit

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
import android.widget.EditText
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_create_item.view.itemNameEditText
import pl.szymonchaber.groupshopping.R

class EditItemDialog : BottomSheetDialogFragment() {

    private lateinit var editText: EditText
    private var editItemListener: OnEditItemListener? = null

    private var itemId: String? = null
    private var currentName: String? = null

    fun setActionsListener(listener: OnEditItemListener) {
        editItemListener = listener
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        itemId = arguments?.getString(KEY_ITEM_ID)
        currentName = arguments?.getString(KEY_CURRENT_NAME)
        return BottomSheetDialog(requireActivity(), theme).apply {
            setContentView(createView())
            window?.setSoftInputMode(SOFT_INPUT_ADJUST_RESIZE)
        }
    }

    private fun createView(): View {
        return requireActivity().layoutInflater.inflate(R.layout.dialog_create_item, null, false).also {
            editText = it.itemNameEditText
            if (itemId != null && currentName != null) {
                editText.setText(currentName)
            }
            it.findViewById<View>(R.id.acceptButton).setOnClickListener {
                onAcceptClick()
                dismiss()
            }
        }
    }

    private fun onAcceptClick() {
        itemId?.let {
            editItemListener?.onEditItem(editText.text.toString(), it)
        } ?: editItemListener?.onAddItem(editText.text.toString())
    }

    companion object {

        private const val KEY_ITEM_ID = "itemId"
        private const val KEY_CURRENT_NAME = "currentName"

        fun create(itemId: String?, currentName: String?): EditItemDialog {
            return EditItemDialog().apply {
                arguments = Bundle().apply {
                    itemId?.let { putString(KEY_ITEM_ID, it) }
                    currentName?.let { putString(KEY_CURRENT_NAME, it) }
                }
            }
        }
    }
}
