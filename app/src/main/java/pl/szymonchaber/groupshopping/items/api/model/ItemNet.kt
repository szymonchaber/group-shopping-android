package pl.szymonchaber.groupshopping.items.api.model

import java.io.Serializable

data class ItemNet(
    val created: String?,
    val isTaken: Boolean,
    val name: String,
    val id: String,
    val order: Int
) : Serializable
