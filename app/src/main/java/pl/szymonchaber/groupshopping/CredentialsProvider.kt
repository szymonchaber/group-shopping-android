package pl.szymonchaber.groupshopping

interface CredentialsProvider {

    fun getBasicAuth(): String
}