package pl.szymonchaber.groupshopping.login.api.model

import java.io.Serializable

data class CreateUserPayload(val username: String, val password: String) : Serializable
