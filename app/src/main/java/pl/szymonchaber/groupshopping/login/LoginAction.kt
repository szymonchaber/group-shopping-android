package pl.szymonchaber.groupshopping.login

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Credentials
import pl.szymonchaber.groupshopping.CredentialsProvider
import pl.szymonchaber.groupshopping.common.api.action.UnathorizedAction
import pl.szymonchaber.groupshopping.common.api.service.ServiceGenerator
import pl.szymonchaber.groupshopping.login.api.AuthorizationService
import retrofit2.Response

class LoginAction : UnathorizedAction() {

    fun performLogin(username: String, password: String): Observable<Response<Void>> {
        return ServiceGenerator.createAuthorizedService(
            AuthorizationService::class.java,
            object : CredentialsProvider {
                override fun getBasicAuth() = Credentials.basic(username, password)
            }).verifyUser()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
