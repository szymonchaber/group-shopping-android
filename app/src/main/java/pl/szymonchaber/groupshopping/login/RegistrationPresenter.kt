package pl.szymonchaber.groupshopping.login

import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.login.api.CreateAccountAction

class RegistrationPresenter(
    private val registrationView: RegistrationView,
    private val credentialsManager: CredentialsManager,
    private val createAccountAction: CreateAccountAction = CreateAccountAction()
) {

    fun createAccount(username: String, password: String) {
        createAccountAction.getCreateAccountObservable(username, password)
            .subscribe({
                if (it.isSuccessful) {
                    credentialsManager.setBasicAuth(username, password)
                    registrationView.onRegistrationSuccess()
                } else {
                    registrationView.onRegistrationFailure()
                }
            }, {
                registrationView.onRegistrationFailure()
            })
    }

    interface RegistrationView {

        fun onRegistrationSuccess()
        fun onRegistrationFailure()
    }
}
