package pl.szymonchaber.groupshopping.login

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_login.login_progress
import kotlinx.android.synthetic.main.activity_login.password
import kotlinx.android.synthetic.main.activity_registration.content
import kotlinx.android.synthetic.main.activity_registration.createAccount
import kotlinx.android.synthetic.main.activity_registration.emailInput
import kotlinx.android.synthetic.main.activity_registration.registrationForm
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.lists.ListsActivity

class RegistrationActivity : AppCompatActivity(), RegistrationPresenter.RegistrationView {

    private lateinit var presenter: RegistrationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        title = getString(R.string.registration)
        presenter = RegistrationPresenter(this, CredentialsManager(this))
        createAccount.setOnClickListener { attemptAccountCreation() }
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptAccountCreation()
                return@OnEditorActionListener true
            }
            false
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRegistrationFailure() {
        Snackbar.make(content, R.string.registration_failure, Snackbar.LENGTH_LONG).show()
    }

    private fun attemptAccountCreation() {
        emailInput.error = null
        password.error = null

        val emailStr = emailInput.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        if (TextUtils.isEmpty(emailStr)) {
            emailInput.error = getString(R.string.error_field_required)
            focusView = emailInput
            cancel = true
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
            emailInput.error = getString(R.string.error_invalid_email)
            focusView = emailInput
            cancel = true
        }

        if (cancel) {
            focusView?.requestFocus()
        } else {
            showProgress(true)
            presenter.createAccount(emailStr, passwordStr)
        }
    }

    override fun onRegistrationSuccess() {
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.SIGN_UP, null)

        startActivity(Intent(this, ListsActivity::class.java))
        finish()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    private fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

        registrationForm.visibility = if (show) View.GONE else View.VISIBLE
        registrationForm.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 0 else 1).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    registrationForm.visibility = if (show) View.GONE else View.VISIBLE
                }
            })

        login_progress.visibility = if (show) View.VISIBLE else View.GONE
        login_progress.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 1 else 0).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    login_progress.visibility = if (show) View.VISIBLE else View.GONE
                }
            })
    }
}
