package pl.szymonchaber.groupshopping.login.api.model

import java.io.Serializable

data class UserNet(val username: String) : Serializable
