package pl.szymonchaber.groupshopping.login.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.common.api.action.UnathorizedAction
import pl.szymonchaber.groupshopping.login.api.model.CreateUserPayload
import retrofit2.Response

class CreateAccountAction : UnathorizedAction() {

    fun getCreateAccountObservable(username: String, password: String): Observable<Response<Void>> {
        return create(AuthorizationService::class.java).createAccount(
            CreateUserPayload(username, password)
        )
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}
