package pl.szymonchaber.groupshopping.login.api

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.login.api.model.CreateUserPayload
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthorizationService {

    @POST("createuser/")
    fun createAccount(@Body user: CreateUserPayload): Observable<Response<Void>>

    @GET("verify/")
    fun verifyUser(): Observable<Response<Void>>
}
