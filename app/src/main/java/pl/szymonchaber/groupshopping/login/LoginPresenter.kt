package pl.szymonchaber.groupshopping.login

import pl.szymonchaber.groupshopping.CredentialsManager
import java.net.HttpURLConnection

class LoginPresenter(
    private val credentialsManager: CredentialsManager,
    private val view: LoginView,
    private val loginAction: LoginAction = LoginAction()
) {

    fun attemptLogin(username: String, password: String) {
        val usernameTrimmed = username.trim()
        val passwordTrimmed = password.trim()

        if (usernameTrimmed.isEmpty() || passwordTrimmed.isEmpty()) {
            view.onLoginFailure()
            return
        }

        loginAction.performLogin(usernameTrimmed, passwordTrimmed).subscribe({
            if (it.isSuccessful) {
                credentialsManager.setBasicAuth(usernameTrimmed, passwordTrimmed)
                view.onLoginSuccessful()
            } else {
                if (it.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    view.onLoginFailure()
                } else {
                    view.onServerError()
                }
            }
        }, {
            view.onServerError()
        })
    }

    interface LoginView {

        fun onLoginSuccessful()
        fun onLoginFailure()
        fun onServerError()
    }
}
