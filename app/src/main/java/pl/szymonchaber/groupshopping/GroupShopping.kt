package pl.szymonchaber.groupshopping

import android.app.Application
import androidx.room.Room
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import pl.szymonchaber.groupshopping.database.GroupShoppingDatabase

class GroupShopping : Application() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        database = Room.databaseBuilder(this, GroupShoppingDatabase::class.java, "group-shopping-db").build()
    }

    companion object {

        var database: GroupShoppingDatabase? = null

        fun shoppingListDao() = database?.shoppingListDao()
    }
}
