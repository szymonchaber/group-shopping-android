package pl.szymonchaber.groupshopping

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.google.firebase.analytics.FirebaseAnalytics
import okhttp3.Credentials
import pl.szymonchaber.groupshopping.common.api.service.UserNotLoggedInException
import pl.szymonchaber.groupshopping.tracking.FirebaseParams.IS_LOGGED_IN

class CredentialsManager(context: Context) : CredentialsProvider {

    private var firebaseAnalytics : FirebaseAnalytics = FirebaseAnalytics.getInstance(context)

    private val credentialsPreferences: SharedPreferences = context.getSharedPreferences(PREFS_CREDENTIALS, MODE_PRIVATE)

    override fun getBasicAuth(): String {
        if (!isLoggedIn()) throw UserNotLoggedInException()
        return credentialsPreferences.getString(KEY_AUTH, null)!!
    }

    fun setBasicAuth(username: String, password: String) {
        trackLoggedIn(true)
        credentialsPreferences.edit().putString(KEY_AUTH, Credentials.basic(username, password))
            .putString(KEY_USERNAME, username).apply()
    }

    fun isLoggedIn() = credentialsPreferences.contains(KEY_AUTH)

    fun getUsername(): String = credentialsPreferences.getString(KEY_USERNAME, "") ?: ""

    fun logOut() {
        trackLoggedIn(false)
        credentialsPreferences.edit().clear().apply()
    }

    private fun trackLoggedIn(isLoggedIn: Boolean) {
        firebaseAnalytics.setUserProperty(IS_LOGGED_IN, isLoggedIn.toString())
    }

    companion object {

        private const val PREFS_CREDENTIALS: String = "credentials"
        private const val KEY_AUTH: String = "authorization"
        private const val KEY_USERNAME: String = "username"
    }
}
