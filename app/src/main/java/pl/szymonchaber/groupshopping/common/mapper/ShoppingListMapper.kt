package pl.szymonchaber.groupshopping.common.mapper

import pl.szymonchaber.groupshopping.common.api.model.ShoppingListNet
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.common.model.User
import pl.szymonchaber.groupshopping.database.model.ItemDb
import pl.szymonchaber.groupshopping.database.model.ShoppingListWithItems
import pl.szymonchaber.groupshopping.items.api.model.ItemNet
import pl.szymonchaber.groupshopping.login.api.model.UserNet

object ShoppingListMapper {

    fun fromNetworkShoppingList(networkShoppingList: ShoppingListNet): ShoppingList {

        val localItems = networkShoppingList.items.map {
            fromNetworkItem(it)
        }

        val localUsers = networkShoppingList.users.map {
            fromNetworkUser(it)
        }

        return ShoppingList(networkShoppingList.id, networkShoppingList.title, localItems, localUsers)
    }

    private fun fromNetworkUser(user: UserNet): User {
        return User(user.username)
    }

    private fun fromNetworkItem(item: ItemNet): Item {
        return Item(item.created, item.isTaken, item.name, item.id, item.order)
    }

    fun fromDatabaseShoppingList(databaseShoppingList: ShoppingListWithItems): ShoppingList {

        val localItems = databaseShoppingList.items.map {
            fromDatabaseItem(it)
        }.sortedBy(Item::order)

        val shoppingList = databaseShoppingList.shoppingListDb
        return ShoppingList(shoppingList.id.toString(), shoppingList.title, localItems, emptyList())
    }

    private fun fromDatabaseItem(item: ItemDb): Item {
        return Item(item.created, item.isTaken, item.name, item.id.toString(), item.order)
    }
}
