package pl.szymonchaber.groupshopping.common.model

import java.io.Serializable

data class User(val username: String) : Serializable
