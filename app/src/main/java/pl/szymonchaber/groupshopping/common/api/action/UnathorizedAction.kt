package pl.szymonchaber.groupshopping.common.api.action

import pl.szymonchaber.groupshopping.common.api.service.ServiceGenerator

abstract class UnathorizedAction : BaseAction {

    override fun <S> create(serviceClass: Class<S>) =
        ServiceGenerator.createUnauthorizedService(serviceClass)
}
