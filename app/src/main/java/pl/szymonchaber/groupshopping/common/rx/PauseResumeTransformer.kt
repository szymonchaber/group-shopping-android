package pl.szymonchaber.groupshopping.common.rx

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer

// Unsubscribe from source stream on Pause event and subscribe on Resume event
class PauseResumeTransformer<T>(private val interruptEvent: Observable<InterruptEvent>) :
    ObservableTransformer<T, T> {

    override fun apply(source: Observable<T>): ObservableSource<T> {
        return Observable.switchOnNext(interruptEvent.map {
            if (it == InterruptEvent.RESUME) source
            else Observable.never()
        })
    }
}
