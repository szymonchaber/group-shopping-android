package pl.szymonchaber.groupshopping.common.api.action

import pl.szymonchaber.groupshopping.CredentialsProvider
import pl.szymonchaber.groupshopping.common.api.service.ServiceGenerator

abstract class AuthorizedAction(private val credentialsProvider: CredentialsProvider) : BaseAction {

    override fun <S> create(serviceClass: Class<S>) =
        ServiceGenerator.createAuthorizedService(serviceClass, credentialsProvider)
}
