package pl.szymonchaber.groupshopping.common.rx

enum class InterruptEvent {

    PAUSE, RESUME
}
