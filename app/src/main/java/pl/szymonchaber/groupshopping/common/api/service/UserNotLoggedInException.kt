package pl.szymonchaber.groupshopping.common.api.service

class UserNotLoggedInException : Exception("There are no performLogin credentials provided")
