package pl.szymonchaber.groupshopping.common.model

import java.io.Serializable

data class ShoppingList(
    val id: String,
    val title: String,
    val items: List<Item>,
    val users: List<User>
) : Serializable
