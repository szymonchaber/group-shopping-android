package pl.szymonchaber.groupshopping.common.model

import java.io.Serializable

data class Item(
    val created: String?,
    val isTaken: Boolean,
    val name: String,
    val id: String,
    val order: Int
) : Serializable
