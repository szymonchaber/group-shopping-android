package pl.szymonchaber.groupshopping.common.api.model

import com.google.gson.annotations.SerializedName
import pl.szymonchaber.groupshopping.items.api.model.ItemNet
import pl.szymonchaber.groupshopping.login.api.model.UserNet
import java.io.Serializable

data class ShoppingListNet(
        val id: String,
        val title: String,
        @SerializedName("items") private val _items: List<ItemNet>?,
        @SerializedName("users") private val _users: List<UserNet>?
) : Serializable {

    val items: List<ItemNet>
        get() = _items ?: ArrayList()

    val users: List<UserNet>
        get() = _users ?: ArrayList()
}
