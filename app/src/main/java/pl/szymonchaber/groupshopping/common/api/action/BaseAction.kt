package pl.szymonchaber.groupshopping.common.api.action

internal interface BaseAction {

    fun <S> create(serviceClass: Class<S>): S
}
