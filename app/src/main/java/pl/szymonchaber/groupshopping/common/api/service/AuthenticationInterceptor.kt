package pl.szymonchaber.groupshopping.common.api.service

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val authToken: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request().newBuilder().header("Authorization", authToken).build())
    }
}
