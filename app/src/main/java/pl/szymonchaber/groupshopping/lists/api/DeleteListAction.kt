package pl.szymonchaber.groupshopping.lists.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsProvider
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import retrofit2.Response

class DeleteListAction(credentialsProvider: CredentialsProvider) : AuthorizedAction(credentialsProvider) {

    fun getDeleteListObservable(shoppingListId: String): Observable<Response<Void>> {
        return create(ShoppingListsService::class.java)
                .deleteShoppingList(shoppingListId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
