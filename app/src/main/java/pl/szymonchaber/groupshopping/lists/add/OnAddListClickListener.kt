package pl.szymonchaber.groupshopping.lists.add

interface OnAddListClickListener {

    fun onAddList(title: String)
}
