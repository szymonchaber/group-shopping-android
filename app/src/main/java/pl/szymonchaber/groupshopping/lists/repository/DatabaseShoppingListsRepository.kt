package pl.szymonchaber.groupshopping.lists.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.GroupShopping
import pl.szymonchaber.groupshopping.common.mapper.ShoppingListMapper
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.database.model.ShoppingListDb as DbShoppingList

class DatabaseShoppingListsRepository : ShoppingListsRepository() {

    override fun getShoppingLists(): Observable<List<ShoppingList>> {
        return GroupShopping.database?.shoppingListDao()?.getShoppingLists()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
                ?.map {
                    it.map { ShoppingListMapper.fromDatabaseShoppingList(it) }
                }?.flatMapObservable { Observable.just(it) } ?: Observable.just(emptyList())
    }

    override fun addShoppingList(title: String): Observable<Any> {
        return Observable.fromCallable {
            GroupShopping.database?.shoppingListDao()?.insertShoppingList(DbShoppingList(id = 0, title = title))
            return@fromCallable Any()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun deleteShoppingList(shoppingListId: String): Observable<Any> {
        return Observable.fromCallable {
            GroupShopping.database?.shoppingListDao()?.deleteShoppingList(shoppingListId.toLong())
            return@fromCallable Any()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

