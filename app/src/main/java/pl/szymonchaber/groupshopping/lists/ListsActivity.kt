package pl.szymonchaber.groupshopping.lists

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_list.content
import kotlinx.android.synthetic.main.activity_list.emptyView
import kotlinx.android.synthetic.main.activity_shopping_lists.addListButton
import kotlinx.android.synthetic.main.activity_shopping_lists.listsRecyclerView
import kotlinx.android.synthetic.main.activity_shopping_lists.progress
import kotlinx.android.synthetic.main.activity_shopping_lists.retry
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.items.ListDetailActivity
import pl.szymonchaber.groupshopping.items.RecyclerItemTouchHelper
import pl.szymonchaber.groupshopping.lists.add.AddListDialog
import pl.szymonchaber.groupshopping.lists.add.OnAddListClickListener
import pl.szymonchaber.groupshopping.login.LoginActivity
import pl.szymonchaber.groupshopping.tracking.CustomEvents
import pl.szymonchaber.groupshopping.tracking.FirebaseParams
import java.io.Serializable

class ListsActivity : AppCompatActivity(),
        ShoppingListsView,
        OnAddListClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private lateinit var presenter: ListsPresenter
    private lateinit var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    private lateinit var listAdapter: ListsAdapter

    private lateinit var credentialsManager: CredentialsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_lists)
        supportActionBar?.setTitle(R.string.title_activity_shopping_lists)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        credentialsManager = CredentialsManager(this)
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        listAdapter = ListsAdapter({ showList(it) }, { presenter.onDeleteList(it) })
        listsRecyclerView.adapter = listAdapter
        listsRecyclerView.addItemDecoration(
            androidx.recyclerview.widget.DividerItemDecoration(
                this,
                androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
            )
        )
        setupSwipeToDelete()
        addListButton.setOnClickListener {
            showAddListDialog()
        }
        retry.setOnClickListener {
            hideContent()
            showProgress()
            presenter.load()
        }
        presenter = ListsPresenter(this, credentialsManager)
        showProgress()
        savedInstanceState?.let {
            presenter.restoreState(
                    savedInstanceState.getSerializable(KEY_SHOPPING_LISTS) as List<ShoppingList>,
                    savedInstanceState.getSerializable(KEY_FILTERED_LISTS) as List<String>
            )
        } ?: presenter.load()
        swipeRefreshLayout.setOnRefreshListener {
            if (retry.visibility == View.VISIBLE) {
                hideContent()
                hideRetryButton()
                swipeRefreshLayout.isRefreshing = false
                showProgress()
            }
            presenter.onSwipeToRefresh()
        }
    }

    private fun reloadPresenter() {
        listAdapter.swapData(emptyList())
        showEmptyView()
        presenter = ListsPresenter(
                this,
                credentialsManager
        )
        presenter.load()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LOGIN_REQUEST_CODE) {
            invalidateOptionsMenu()
            reloadPresenter()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupSwipeToDelete() {
        val itemTouchHelperCallback = RecyclerItemTouchHelper(this, false)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(listsRecyclerView)
    }

    override fun onItemDismiss(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ListsAdapter.ListViewHolder) {
            firebaseAnalytics.logEvent(CustomEvents.DELETE_SHOPPING_LIST, null)
            presenter.onDeleteList(viewHolder.adapterPosition)
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        //nop
    }

    override fun onDragStart() {
        //nop
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.lists_menu, menu)
        menu.findItem(R.id.logOut).title =
                if (credentialsManager.isLoggedIn()) getString(R.string.logout) else getString(R.string.login)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logOut -> {
                if (credentialsManager.isLoggedIn()) {
                    logout()
                } else {
                    login()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun login() {
        firebaseAnalytics.logEvent(CustomEvents.MENU_LOGIN_CLICK, null)
        startActivityForResult(Intent(this, LoginActivity::class.java), LOGIN_REQUEST_CODE)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(KEY_SHOPPING_LISTS, presenter.getShoppingLists() as Serializable)
        outState.putSerializable(KEY_FILTERED_LISTS, presenter.getFilteredLists() as Serializable)
    }

    private fun logout() {
        firebaseAnalytics.logEvent(CustomEvents.LOGOUT, null)
        credentialsManager.logOut()
        invalidateOptionsMenu()
        reloadPresenter()
    }

    private fun showAddListDialog() {
        firebaseAnalytics.logEvent(CustomEvents.CLICK_ADD_SHOPPING_LIST, null)
        val addListDialog = AddListDialog()
        addListDialog.show(supportFragmentManager, "AddListDialog")
        addListDialog.setActionsListener(this)
    }

    private fun showList(item: ShoppingList) {
        startActivity(ListDetailActivity.createIntent(this, item))
    }

    override fun show(shoppingListModel: List<ShoppingList>) {
        firebaseAnalytics.setUserProperty(FirebaseParams.LISTS_COUNT, shoppingListModel.size.toString())
        listsRecyclerView.visibility = View.VISIBLE
        listAdapter.swapData(shoppingListModel)
        swipeRefreshLayout.isRefreshing = false
    }

    override fun hideContent() {
        listsRecyclerView.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun hideRetryButton() {
        retry.visibility = View.GONE
    }

    override fun onError(throwable: Throwable) {
        Snackbar.make(swipeRefreshLayout, R.string.server_error, Snackbar.LENGTH_SHORT).show()
        swipeRefreshLayout.isRefreshing = false
        retry.visibility = View.VISIBLE
    }

    override fun onAddList(title: String) {
        firebaseAnalytics.logEvent(CustomEvents.ADD_SHOPPING_LIST, Bundle().apply {
            putInt(FirebaseParams.LIST_TITLE_LENGTH, title.length)
        })
        presenter.onAddList(title)
    }

    override fun showShoppingListDeleted(shoppingListPosition: Int) {
        listAdapter.removeList(shoppingListPosition)
    }

    override fun showUndoSnackbar(shoppingListToDelete: ShoppingList, shoppingListPosition: Int) {
        Snackbar.make(content, formatListTitle(shoppingListToDelete.title), Snackbar.LENGTH_LONG)
                .setAction(R.string.undo) {
                    listAdapter.restoreList(shoppingListToDelete, shoppingListPosition)
                    presenter.undoDeleting(shoppingListToDelete)
                }.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        if (event != DISMISS_EVENT_ACTION) presenter.onDeleteItemSnackbarDismissed(shoppingListToDelete)
                    }
                }).setActionTextColor(Color.YELLOW).show()
    }

    private fun formatListTitle(title: String): String {
        return if (title.length < 15) getString(R.string.list_deleted, title) else
            getString(
                    R.string.list_deleted_ellipsized, title.substring(0, 15)
            )
    }

    override fun onDeleteListFailed() {
        Snackbar.make(content, R.string.list_unable_to_delete_list, Snackbar.LENGTH_SHORT).show()
    }

    companion object {

        private const val KEY_SHOPPING_LISTS = "shopping_lists"
        private const val KEY_FILTERED_LISTS = "filtered_lists"
        private const val LOGIN_REQUEST_CODE = 1001
    }
}
