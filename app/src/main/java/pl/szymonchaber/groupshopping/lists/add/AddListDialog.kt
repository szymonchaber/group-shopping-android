package pl.szymonchaber.groupshopping.lists.add

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import pl.szymonchaber.groupshopping.R

class AddListDialog : AppCompatDialogFragment() {

    private lateinit var editText: EditText
    private var addListListener: OnAddListClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireActivity())
                .setTitle(R.string.lists_add_list)
                .setView(createView())
                .setPositiveButton(R.string.action_add) { _, _ ->
                    addListListener?.onAddList(editText.text.toString())
                }
                .setNegativeButton(R.string.action_cancel, null)
                .create()
                .apply {
                    window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
                }
    }

    fun setActionsListener(listener: OnAddListClickListener) {
        addListListener = listener
    }

    private fun createView(): View {
        return requireActivity().layoutInflater.inflate(R.layout.dialog_create_list, null, false).also {
            editText = it.findViewById(R.id.titleEditText)
        }
    }
}
