package pl.szymonchaber.groupshopping.lists.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.items.api.model.EditShoppingList

class AddShoppingListAction(credentialsManager: CredentialsManager) : AuthorizedAction(credentialsManager) {

    fun getAddShoppingListObservable(title: String): Observable<Any> {
        return create(ShoppingListsService::class.java)
            .addShoppingList(EditShoppingList(title))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
