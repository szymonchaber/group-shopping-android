package pl.szymonchaber.groupshopping.lists.api

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.api.action.AuthorizedAction
import pl.szymonchaber.groupshopping.common.api.model.ShoppingListNet

class GetShoppingListsAction(credentialsManager: CredentialsManager) : AuthorizedAction(credentialsManager) {

    fun getShoppingListsObservable(): Observable<List<ShoppingListNet>> {
        return create(ShoppingListsService::class.java)
            .getShoppingLists()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

