package pl.szymonchaber.groupshopping.lists.repository

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.ShoppingList

abstract class ShoppingListsRepository {

    abstract fun getShoppingLists(): Observable<List<ShoppingList>>
    abstract fun addShoppingList(title: String): Observable<Any>
    abstract fun deleteShoppingList(shoppingListId: String): Observable<Any>

    companion object {

        fun from(credentialsManager: CredentialsManager): ShoppingListsRepository {
            return if (credentialsManager.isLoggedIn()) {
                NetworkShoppingListsRepository(credentialsManager)
            } else {
                DatabaseShoppingListsRepository()
            }
        }
    }
}
