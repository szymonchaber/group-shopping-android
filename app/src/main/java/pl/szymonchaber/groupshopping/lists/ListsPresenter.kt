package pl.szymonchaber.groupshopping.lists

import android.util.Log
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.lists.repository.ShoppingListsRepository

class ListsPresenter(
        private val view: ShoppingListsView,
        private val credentialsManager: CredentialsManager,
        private val shoppingListsRepository: ShoppingListsRepository = ShoppingListsRepository.from(
        credentialsManager
    )
) {

    private var shoppingLists: List<ShoppingList> = ArrayList()
    private val filteredLists: MutableList<String> = ArrayList()

    fun onSwipeToRefresh() {
        load()
    }

    fun load() {
        shoppingListsRepository.getShoppingLists()
            .doOnSubscribe { view.hideRetryButton() }
            .subscribe({ list ->
                onLoaded(list)
            }, this::onError)
    }

    fun restoreState(shoppingLists: List<ShoppingList>, filteredLists: List<String>) {
        this.shoppingLists = shoppingLists
        this.filteredLists.apply {
            clear()
            addAll(filteredLists)
        }
        onLoaded(shoppingLists)
    }

    private fun onLoaded(list: List<ShoppingList>) {
        view.hideProgress()
        shoppingLists = list.filterNot { filteredLists.contains(it.id) }
        view.show(shoppingLists)
        if (shoppingLists.isEmpty()) {
            view.showEmptyView()
        } else {
            view.hideEmptyView()
        }
    }

    fun onAddList(title: String) {
        with(title.trim()) {
            if (this == "") return
            else shoppingListsRepository.addShoppingList(this).subscribe(
                { load() },
                this@ListsPresenter::onError
            )
        }
    }

    private fun onError(throwable: Throwable) {
        Log.e("ListsPresenter", "error:", throwable)
        view.hideProgress()
        view.hideContent()
        view.hideEmptyView()
        view.onError(throwable)
    }

    fun onDeleteList(shoppingListPosition: Int) {
        val shoppingListToDelete = shoppingLists[shoppingListPosition]
        filteredLists.add(shoppingListToDelete.id)
        view.showShoppingListDeleted(shoppingListPosition)
        view.showUndoSnackbar(shoppingListToDelete, shoppingListPosition)
    }

    fun onDeleteItemSnackbarDismissed(shoppingList: ShoppingList) {
        performDeleteRequest(shoppingList.id)
    }

    private fun performDeleteRequest(shoppingListId: String) {
        shoppingListsRepository.deleteShoppingList(shoppingListId).subscribe(
            {
                onDeleteListActionFinished(shoppingListId)

            },
            {
                view.onDeleteListFailed()
                onDeleteListActionFinished(shoppingListId)
            })
    }

    private fun onDeleteListActionFinished(shoppingListId: String) {
        filteredLists.remove(shoppingListId)
        load()
    }

    fun undoDeleting(shoppingList: ShoppingList) {
        filteredLists.remove(shoppingList.id)
    }

    fun getShoppingLists(): List<ShoppingList> {
        return shoppingLists
    }

    fun getFilteredLists(): List<String> {
        return filteredLists
    }
}
