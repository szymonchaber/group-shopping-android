package pl.szymonchaber.groupshopping.lists.repository

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.mapper.ShoppingListMapper
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.lists.api.AddShoppingListAction
import pl.szymonchaber.groupshopping.lists.api.DeleteListAction
import pl.szymonchaber.groupshopping.lists.api.GetShoppingListsAction

class NetworkShoppingListsRepository(
        credentialsManager: CredentialsManager,
        private val getShoppingListsAction: GetShoppingListsAction = GetShoppingListsAction(
                credentialsManager
        ),
        private val addShoppingListAction: AddShoppingListAction = AddShoppingListAction(
                credentialsManager
        ),
        private val deleteListAction: DeleteListAction = DeleteListAction(
                credentialsManager
        )

) : ShoppingListsRepository() {

    override fun getShoppingLists(): Observable<List<ShoppingList>> {
        return getShoppingListsAction.getShoppingListsObservable()
                .map { it.map { ShoppingListMapper.fromNetworkShoppingList(it) } }
    }

    override fun addShoppingList(title: String): Observable<Any> {
        return addShoppingListAction.getAddShoppingListObservable(title)
    }

    override fun deleteShoppingList(shoppingListId: String): Observable<Any> {
        return deleteListAction.getDeleteListObservable(shoppingListId).flatMap { it ->
            if (it.isSuccessful) Observable.just(Any()) else Observable.error(Error())
        }
    }
}
