package pl.szymonchaber.groupshopping.lists.api

import io.reactivex.Observable
import pl.szymonchaber.groupshopping.common.api.model.ShoppingListNet
import pl.szymonchaber.groupshopping.items.api.model.EditShoppingList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ShoppingListsService {

    @GET("lists/")
    fun getShoppingLists(): Observable<List<ShoppingListNet>>

    @POST("lists/")
    fun addShoppingList(@Body shoppingList: EditShoppingList): Observable<Any>

    @DELETE("lists/{id}/")
    fun deleteShoppingList(@Path("id") id: String): Observable<Response<Void>>
}
