package pl.szymonchaber.groupshopping.lists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.szymonchaber.groupshopping.R
import pl.szymonchaber.groupshopping.common.model.ShoppingList

class ListsAdapter(
        private val onListClickListener: (list: ShoppingList) -> Unit,
        private val onListDeleteClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ListsAdapter.ListViewHolder>() {

    private var data: MutableList<ShoppingList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.shopping_list, parent, false),
                onListClickListener, onListDeleteClickListener
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) = holder.bind(data[position])

    fun swapData(data: List<ShoppingList>) {
        this.data = data.toMutableList()
        notifyDataSetChanged()
    }

    fun removeList(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreList(list: ShoppingList, position: Int) {
        data.add(position, list)
        notifyItemInserted(position)
    }

    class ListViewHolder(
            view: View,
            private val onListClickListener: (list: ShoppingList) -> Unit,
            private val onListDeleteClickListener: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private val title: TextView = view.findViewById(R.id.title)
        private val viewBackground: View = view.findViewById(R.id.view_background)
        val viewForeground: View = view.findViewById(R.id.view_foreground)

        init {
            viewBackground.setOnClickListener {
                onListDeleteClickListener.invoke(adapterPosition)
            }
        }

        fun bind(item: ShoppingList) {
            title.text = item.title

            viewForeground.setOnClickListener {
                onListClickListener.invoke(item)
            }
        }
    }
}
