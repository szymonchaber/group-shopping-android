package pl.szymonchaber.groupshopping.lists

import pl.szymonchaber.groupshopping.common.model.ShoppingList

interface ShoppingListsView {

    fun show(shoppingListModel: List<ShoppingList>)
    fun onError(throwable: Throwable)
    fun showShoppingListDeleted(shoppingListPosition: Int)
    fun showUndoSnackbar(shoppingListToDelete: ShoppingList, shoppingListPosition: Int)
    fun onDeleteListFailed()
    fun showEmptyView()
    fun hideEmptyView()
    fun showProgress()
    fun hideProgress()
    fun hideRetryButton()
    fun hideContent()
}
