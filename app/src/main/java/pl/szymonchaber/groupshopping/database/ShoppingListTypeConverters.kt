package pl.szymonchaber.groupshopping.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pl.szymonchaber.groupshopping.database.model.ItemDb
import java.util.Collections

class ShoppingListTypeConverters {

    var gson = Gson()

    @TypeConverter
    fun stringToItemList(data: String?): List<ItemDb> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<ItemDb>>() {

        }.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun itemListToString(items: List<ItemDb>): String {
        return gson.toJson(items)
    }
}
