package pl.szymonchaber.groupshopping.database.model

import androidx.room.Embedded
import androidx.room.Relation

class ShoppingListWithItems {

    @Embedded
    lateinit var shoppingListDb: ShoppingListDb

    @Relation(parentColumn = "id", entityColumn = "listId")
    lateinit var items: List<ItemDb>
}
