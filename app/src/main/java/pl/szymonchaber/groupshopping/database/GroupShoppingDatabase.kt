package pl.szymonchaber.groupshopping.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.szymonchaber.groupshopping.database.model.ItemDb
import pl.szymonchaber.groupshopping.database.model.ShoppingListDb

@Database(entities = [ShoppingListDb::class, ItemDb::class], version = 3, exportSchema = true)
@TypeConverters(ShoppingListTypeConverters::class)
abstract class GroupShoppingDatabase : RoomDatabase() {

    abstract fun shoppingListDao(): ShoppingListDao
}
