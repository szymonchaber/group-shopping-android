package pl.szymonchaber.groupshopping.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(
    foreignKeys = [(ForeignKey(
        entity = ShoppingListDb::class,
        parentColumns = ["id"],
        childColumns = ["listId"],
        onDelete = CASCADE
    ))]
)
data class ItemDb(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val isTaken: Boolean = false,
    val created: String = "",
    val listId: Long,
    @ColumnInfo(name = "ordering", index = true)
    val order: Int = 0
) : Serializable
