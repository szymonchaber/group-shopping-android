package pl.szymonchaber.groupshopping.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class ShoppingListDb(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val title: String = ""
) : Serializable
