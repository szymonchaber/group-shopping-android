package pl.szymonchaber.groupshopping.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Maybe
import pl.szymonchaber.groupshopping.database.model.ItemDb
import pl.szymonchaber.groupshopping.database.model.ShoppingListDb
import pl.szymonchaber.groupshopping.database.model.ShoppingListWithItems

@Dao
interface ShoppingListDao {

    @Query("SELECT * FROM ShoppingListDb")
    fun getShoppingLists(): Maybe<List<ShoppingListWithItems>>

    @Insert
    fun insertShoppingList(shoppingList: ShoppingListDb)

    @Query("DELETE FROM ShoppingListDb WHERE id=:shoppingListId")
    fun deleteShoppingList(shoppingListId: Long)

    @Query("SELECT * FROM ItemDb")
    fun getItems(): Maybe<List<ItemDb>>

    @Insert
    fun insertItem(itemDb: ItemDb)

    @Update
    fun updateItem(itemDb: ItemDb)

    @Query("DELETE FROM ItemDb WHERE id=:itemId")
    fun deleteItem(itemId: Long)

    @Query("DELETE FROM ShoppingListDb")
    fun deleteAll()
}
