package pl.szymonchaber.groupshopping.login

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.login.api.CreateAccountAction
import retrofit2.Response

@RunWith(JUnit4::class)
class RegistrationPresenterTest {

    @RelaxedMockK
    private lateinit var registrationView: RegistrationPresenter.RegistrationView

    @RelaxedMockK
    private lateinit var credentialsManager: CredentialsManager

    @MockK
    private lateinit var createAccountAction: CreateAccountAction

    private lateinit var registrationPresenter: RegistrationPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        registrationPresenter = RegistrationPresenter(registrationView, credentialsManager, createAccountAction)
    }

    @Test
    fun shouldCreateAccountOnProperInput() {
        //given
        val username = "username"
        val password = "password"
        val response = mockk<Response<Void>>()
        every { response.isSuccessful }.returns(true)
        every { createAccountAction.getCreateAccountObservable(any(), any()) }.returns(Observable.just(response))

        //when
        registrationPresenter.createAccount(username, password)

        //then
        verify { createAccountAction.getCreateAccountObservable(username, password) }
        verify { credentialsManager.setBasicAuth(username, password) }
        verify { registrationView.onRegistrationSuccess() }
    }

    @Test
    fun shouldCallOnLoginFailureWhenRequestNotSuccessful() {
        //given
        val response = mockk<Response<Void>>()
        every { response.isSuccessful }.returns(false)
        every { createAccountAction.getCreateAccountObservable(any(), any()) }.returns(Observable.just(response))

        //when
        registrationPresenter.createAccount("username", "password")

        //then
        verify { createAccountAction.getCreateAccountObservable(any(), any()) }
        verify(exactly = 0) { credentialsManager.setBasicAuth(any(), any()) }
        verify { registrationView.onRegistrationFailure() }
    }

    @Test
    fun shouldCallOnLoginFailureWhenExceptionThrown() {
        //given
        every { createAccountAction.getCreateAccountObservable(any(), any()) }.returns(Observable.error(Exception()))

        //when
        registrationPresenter.createAccount("username", "password")

        //then
        verify { createAccountAction.getCreateAccountObservable(any(), any()) }
        verify(exactly = 0) { credentialsManager.setBasicAuth(any(), any()) }
        verify { registrationView.onRegistrationFailure() }
    }
}
