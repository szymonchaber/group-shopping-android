package pl.szymonchaber.groupshopping.login

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pl.szymonchaber.groupshopping.CredentialsManager
import retrofit2.Response
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class LoginPresenterTest {

    @RelaxedMockK
    private lateinit var credentialsManager: CredentialsManager

    @RelaxedMockK
    private lateinit var loginView: LoginPresenter.LoginView

    @RelaxedMockK
    private lateinit var loginAction: LoginAction

    private lateinit var loginPresenter: LoginPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        loginPresenter = LoginPresenter(credentialsManager, loginView, loginAction)
    }

    @Test
    fun shouldLoginWithTrimmedInput() {
        //given
        val untrimmedLogin = "login    "
        val untrimmedPassword = "password    "

        //when
        loginPresenter.attemptLogin(untrimmedLogin, untrimmedPassword)

        //then
        verify { loginAction.performLogin("login", "password") }
    }

    @Test
    fun shouldCallOnLoginSuccess() {
        //given
        val mockResponse = mockk<Response<Void>>()
        every { mockResponse.isSuccessful }.returns(true)
        every { loginAction.performLogin(any(), any()) }.returns(Observable.just(mockResponse))
        val login = "login"
        val password = "password"

        //when
        loginPresenter.attemptLogin(login, password)

        //then
        verify { loginAction.performLogin(login, password) }
        verify { credentialsManager.setBasicAuth(login, password) }
        verify { loginView.onLoginSuccessful() }
    }

    @Test
    fun shouldShowLoginFailureWhenUnauthorized() {
        //given
        val mockResponse = mockk<Response<Void>>()
        every { mockResponse.isSuccessful }.returns(false)
        every { mockResponse.code() }.returns(HttpURLConnection.HTTP_UNAUTHORIZED)
        every { loginAction.performLogin(any(), any()) }.returns(Observable.just(mockResponse))
        val login = "login"
        val password = "password"

        //when
        loginPresenter.attemptLogin(login, password)

        //then
        verify { loginAction.performLogin(login, password) }
        verify { loginView.onLoginFailure() }
    }

    @Test
    fun shouldShowServerErrorWhenCodeUnknown() {
        //given
        val mockResponse = mockk<Response<Void>>()
        every { mockResponse.isSuccessful }.returns(false)
        every { mockResponse.code() }.returns(0)
        every { loginAction.performLogin(any(), any()) }.returns(Observable.just(mockResponse))
        val login = "login"
        val password = "password"

        //when
        loginPresenter.attemptLogin(login, password)

        //then
        verify { loginAction.performLogin(login, password) }
        verify { loginView.onServerError() }
    }

    @Test
    fun shouldShowServerErrorOnException() {
        //given
        every { loginAction.performLogin(any(), any()) }.returns(Observable.error(Exception()))
        val login = "login"
        val password = "password"

        //when
        loginPresenter.attemptLogin(login, password)

        //then
        verify { loginAction.performLogin(login, password) }
        verify { loginView.onServerError() }
    }

    @Test
    fun shouldReturnErrorWhenLoginEmpty() {
        //given
        val emptyLogin = ""
        val password = "password"

        //when
        loginPresenter.attemptLogin(emptyLogin, password)

        //then
        verify(exactly = 0) { loginAction.performLogin(any(), any()) }
        verify { loginView.onLoginFailure() }
    }

    @Test
    fun shouldReturnErrorWhenPasswordEmpty() {
        //given
        val password = ""

        //when
        loginPresenter.attemptLogin("login", password)

        //then
        verify(exactly = 0) { loginAction.performLogin(any(), any()) }
        verify { loginView.onLoginFailure() }
    }
}
