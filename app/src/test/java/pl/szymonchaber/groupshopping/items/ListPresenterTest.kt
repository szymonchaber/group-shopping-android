package pl.szymonchaber.groupshopping.items

import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.Item
import pl.szymonchaber.groupshopping.items.repository.ShoppingListDetailsRepository

@RunWith(JUnit4::class)
class ListPresenterTest {

    @RelaxedMockK
    private lateinit var listDetailView: ListDetailView

    @RelaxedMockK
    private lateinit var credentialsManager: CredentialsManager

    @RelaxedMockK
    private lateinit var repository: ShoppingListDetailsRepository

    private lateinit var listPresenter: ListPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        listPresenter = ListPresenter(listDetailView, credentialsManager, "SomeId", repository)
    }

    @Test
    fun shouldSetItemTakenOnCheckedChangeToTrue() {
        val item = mockk<Item>()

        listPresenter.onTakenChanged(item, true)

        verify { repository.setItemTaken(item, true) }
    }

    @Test
    fun shouldSetItemTakenOnCheckedChangeToFalse() {
        val item = mockk<Item>()

        listPresenter.onTakenChanged(item, false)

        verify { repository.setItemTaken(item, false) }
    }

    @Test
    fun shouldAddItemWhenAddItemCalled() {
        val itemName = "Tomato"

        listPresenter.onAddItem(itemName)

        verify { repository.addItem(itemName) }
    }

    @Test
    fun shouldEditItemName() {
        val item = Item("", false, "oldName", "id", 0)
        listPresenter.restoreState(listOf(item), emptyList())

        val newName = "Tomato"
        listPresenter.onEditItem(newName, "id")

        verify { repository.editItemName(item, newName) }
    }
}
