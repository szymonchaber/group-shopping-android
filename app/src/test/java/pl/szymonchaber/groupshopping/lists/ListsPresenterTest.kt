package pl.szymonchaber.groupshopping.lists

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pl.szymonchaber.groupshopping.CredentialsManager
import pl.szymonchaber.groupshopping.common.model.ShoppingList
import pl.szymonchaber.groupshopping.lists.repository.ShoppingListsRepository

@RunWith(JUnit4::class)
class ListsPresenterTest {

    @RelaxedMockK
    private lateinit var view: ShoppingListsView

    @RelaxedMockK
    private lateinit var shoppingListsRepository: ShoppingListsRepository

    @MockK
    private lateinit var credentialsManager: CredentialsManager

    private lateinit var presenter: ListsPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = ListsPresenter(view, credentialsManager, shoppingListsRepository)
    }

    @Test
    fun shouldLoadListOnSwipeToRefresh() {
        //given
        val shoppingList = mockk<ShoppingList>(relaxed = true)
        every { shoppingListsRepository.getShoppingLists() }.returns(Observable.just(listOf(shoppingList, shoppingList)))

        //when
        presenter.onSwipeToRefresh()

        //then
        verify { view.show(listOf(shoppingList, shoppingList)) }
    }

    @Test
    fun shouldLoadListOnLoad() {
        //given
        val shoppingList = mockk<ShoppingList>(relaxed = true)
        every { shoppingListsRepository.getShoppingLists() }.returns(Observable.just(listOf(shoppingList, shoppingList)))

        //when
        presenter.load()

        //then
        verify { view.show(listOf(shoppingList, shoppingList)) }
    }

    @Test
    fun shouldNotAddListIfInputEmpty() {
        //given
        val emptyListTitle = ""

        //when
        presenter.onAddList(emptyListTitle)

        //then
        verify(exactly = 0) { shoppingListsRepository.addShoppingList(emptyListTitle) }
    }

    @Test
    fun shouldTrimNewListTitle() {
        //given
        val notTrimmedTitle = "title   "

        //when
        presenter.onAddList(notTrimmedTitle)

        //then
        verify { shoppingListsRepository.addShoppingList("title") }
    }
}
