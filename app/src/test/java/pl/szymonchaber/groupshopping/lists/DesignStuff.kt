package pl.szymonchaber.groupshopping.lists

import junit.framework.Assert.assertTrue
import org.junit.Test

class DesignStuff {

    @Test
    fun shouldWork() {

        val myBestRepository = MyBestRepository()

        val matchingAll = myBestRepository.matchingAll {
            nameIs("1")
        }

        assertTrue(matchingAll.first().name == "1")

        val matchingEither = myBestRepository.matchingEither {
            nameIs("0")
            nameIs("3")
        }

        assertTrue(matchingEither.first().name == "3")
    }

}

class MyBestRepository {


    fun matchingAll(block: PredicateFactory.() -> Unit): List<Account> {
        return findMatches(block, AllPredicate())
    }

    fun matchingEither(block: PredicateFactory.() -> Unit): List<Account> {
        return findMatches(block, EitherPredicate())
    }

    private fun findMatches(block: PredicateFactory.() -> Unit, parentPredicate: CollectionPredicate): List<Account> {
        val preparedPredicate = PredicateFactory(parentPredicate).apply(block).parentPredicate
        return listOf(Account("1"), Account("2"), Account("3")).filter { account ->
            preparedPredicate.matches(account)
        }
    }
}

class PredicateFactory(val parentPredicate: CollectionPredicate) {

    fun nameIs(name: String) {
        parentPredicate.add(NameAccountPredicate(name))
    }
}

interface AccountPredicate {

    fun matches(account: Account): Boolean
}

class AllPredicate : CollectionPredicate() {

    override fun matches(predicates: List<AccountPredicate>, account: Account): Boolean {
        return predicates.all { it.matches(account) }
    }
}

class EitherPredicate : CollectionPredicate() {

    override fun matches(predicates: List<AccountPredicate>, account: Account): Boolean {
        return predicates.any { it.matches(account) }
    }
}

abstract class CollectionPredicate : AccountPredicate {

    private val predicates: MutableList<AccountPredicate> = mutableListOf()

    fun add(predicate: AccountPredicate) {
        predicates.add(predicate)
    }

    final override fun matches(account: Account): Boolean {
        return matches(predicates, account)
    }

    abstract fun matches(predicates: List<AccountPredicate>, account: Account): Boolean
}

class NameAccountPredicate(private val name: String) : AccountPredicate {

    override fun matches(account: Account): Boolean {
        return name == account.name
    }
}

data class Account(val name: String)
